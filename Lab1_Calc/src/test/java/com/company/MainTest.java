package com.company;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {


    /*@org.junit.jupiter.api.Test
    @DisplayName("Test Calc")
    void main() {
        System.out.println("Test");
        assertEquals(1,1,"Ok");
    }*/
    @Test
    public void testSum() throws Exception{
        assertEquals(5,Main.sum(2,3), "Slojenie OK");
    }
    @Test
    public void testMin() throws Exception{
        assertEquals(7,Main.min(10,3), "Vichitanie OK");
    }
    @Test
    public void testUmn() throws Exception{
        assertEquals(6,Main.umn(2,3), "Umnojenie OK");
    }
    @Test
    public void testDel() throws Exception{
        assertEquals(4,Main.del(20,5), "Delenie OK");
    }

    /*@ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource({
            "0,    1,   1",
            "1,    2,   3",
            "49,  51, 100",
            "1,  100, 101"
    })
    void add(int a, int b, int out) {
        Main calculator = new Main();
        assertEquals(out, calculator.add(a, b),
                () -> a + " + " + b + " should equal " + out);
    }*/
}